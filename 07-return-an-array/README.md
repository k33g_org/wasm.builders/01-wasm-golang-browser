# Use a json object as parameter

The objective of this seventh exercise is to create a wasm Golang function that return an array. And then call this function from the web page (from JavaScript).

## Complete the source code of `main.go`

Import this package `"syscall/js"` 


Then, create a `GiveMeNumbers` function with the code below:

```golang
func GiveMeNumbers(this js.Value, args []js.Value) interface{} {
	return []interface{} {1, 2, 3, 4, 5}
}
```

Then, add this code to the `main` function:

```golang
js.Global().Set("GiveMeNumbers", js.FuncOf(GiveMeNumbers))
```


Then, update the JavaScript code of `index.html` with the code below to load the wasm file and call the `GiveMeNumbers` function:

```javascript
loadWasm("main.wasm").then(wasm => {

	let dataArray = GiveMeNumbers()
	console.log(dataArray)
	document.querySelector("h1").innerHTML = JSON.stringify(dataArray)

}).catch(error => {
  console.log("ouch", error)
}) 
```

## Build the wasm file with Tinygo

```bash
tinygo build -o main.wasm -target wasm ./main.go
```

## Serve the html page

```bash
node index.js
```

Now you have a webapplication listening on `8080`. Because we are on Gitpod, to get the url of the webapplication, type:

```bash
gp url 8080
```

- You should get an url like this one: `https://8080-ivory-worm-lrfo02ph.ws-eu23.gitpod.io`
- Open it in your browser, you should see this message: **[1,2,3,4,5]**

That's all folks 🙂

