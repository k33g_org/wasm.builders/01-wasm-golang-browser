# Call a Go function

The objective of this third exercise is to create a wasm Golang function. And then call it from the web page (from JavaScript).

## Complete the source code of `main.go`

Import this package `"syscall/js"` 

> **Remark**; this package gives access to the WebAssembly host environment (the browsser).

Then, create a `Hello` function:

```golang
func Hello(this js.Value, args []js.Value) interface{} {
  message := args[0].String() // get the parameters
  return "Hello " + message
}
```

> **Explanations**:
> - The `Hello` function takes two parameters and returns an `interface{}` type. 
> - The function will be called synchronously from Javascript. 
> - The first parameter (`this`) refers to JavaScript's global object. 
> - The second parameter is a slice of `[]js.Value` representing the arguments passed to the Javascript function call.

Then, add this code to the `main` function:

```golang
js.Global().Set("Hello", js.FuncOf(Hello))
```

> **Explanations**:
> - To export the function to the global context, we used the `FuncOf` function: `js.Global().Set("Hello", js.FuncOf(Hello))`. 
> - The `FuncOf` function is used to create a `Func` type.

Then, update the JavaScript code of `index.html` with the code below, after the wasm file is loaded (*):

```javascript
let message = Hello("Bob Morane")
document.querySelector("h1").innerHTML = message
```
> (*) when this promise `loadWasm("main.wasm")` is resolved.

## Build the wasm file with Tinygo

```bash
tinygo build -o main.wasm -target wasm ./main.go
```

## Serve the html page

```bash
node index.js
```

Now you have a webapplication listening on `8080`. Because we are on Gitpod, to get the url of the webapplication, type:

```bash
gp url 8080
```

- You should get an url like this one: `https://8080-ivory-worm-lrfo02ph.ws-eu23.gitpod.io`
- Open it in your browser, you should see this message: **"Hello Bob Morane"**

## One more thing

If you open the console of the browser, you'll read this error message:

```
syscall/js.finalizeRef @ wasm_exec.js:313
```
> This bug is known: https://github.com/tinygo-org/tinygo/issues/1140

No worries, it's easy to "fix" 😁, add this to the `loadWasm` function: `go.importObject.env["syscall/js.finalizeRef"] = () => {}`

```javascript
function loadWasm(path) {
  const go = new Go()
  // 🖐 remove the message: syscall/js.finalizeRef not implemented
  go.importObject.env["syscall/js.finalizeRef"] = () => {}

  return new Promise((resolve, reject) => {
    WebAssembly.instantiateStreaming(fetch(path), go.importObject)
    .then(result => {
      go.run(result.instance)
      resolve(result.instance)
    })
    .catch(error => {
      reject(error)
    })
  })
}

```

Right now, you have almost everything you need in your hands to go further. Let's move on to the next exercise.

