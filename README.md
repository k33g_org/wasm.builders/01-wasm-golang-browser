# WASM, GoLang in your browser

This project is a "self-running" hands-on (you can do it on your own) about how to create wasm programs for the browser with GoLang.

This project is like a sandbox with all you need to start playing with **WASM**, **GoLang** and **TinyGo** (in your browser) right now without installing anything. So, **to get the benefit of that, you have to open this project with [Gitpod](https://www.gitpod.io/)**.

This hands-on is composed of **7 small exercises**.

## Exercises

- `./01-getting-started`: display a message in the browser console when loading wasm GoLang code
- `./02-interacting-with-dom`: add an html tag to the web page  when loading wasm GoLang code
- `./03-call-a-go-function`: create a wasm GoLang function with string parameter that returns a string and call it from JavaScript
- `./04-return-a-json-object`: create a wasm GoLang function that returns a JavaScript object
- `./05-json-object-as-parameter`: create a wasm GoLang function with a json object as a parameter
- `./06-array-as-parameter`: create a wasm GoLang function with an array as a parameter
- `./07-return-an-array`: create a wasm GoLang function that returns an array

## Versions

| Tools  | Versions |
|--------|----------|
| Go     | 1.17.5   |
| TinyGo | 0.21.0   |