# Getting started

The objective of this first exercise is to display a message in the browser console when calling a wasm Golang code.

Almost all the needed files are available:

- `index.js` is the source code to run an http server and serve html content.
- `index.html` is the html page that will load (with JavaScript) the wasm file and run the GoLang code (all the JavaScript code is already available).
- `main.go` is the Go source code that will print a message.

> 👋 everything is "more or less" explained with comments in the source code.

🚀 Go to the exercise directory: `cd 01-getting-started`

## Compile the wasm file

Before compiling the wasm file, get `wasm_exec.js`.

> The `wasm_exec.js` file is provided by the GoLang project. It's a set of JavaScript helper functions which are called from WebAssembly. 🖐 Ensure you are using the same version of `wasm_exec.js` as the version of Go you are using to compile.

Use this command to get it:
```bash
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .
```

**Build** `main.wasm`:

```bash
GOOS=js GOARCH=wasm go build -o main.wasm
```

## Load the wasm file from JavaScript

If you look at the JavaScript code in the `index.html` page, you'll see the `loadWasm` function that uses the JavaScript API `WebAssembly.instantiateStreaming` to load the wasm file:

```javascript
function loadWasm(path) {
  const go = new Go()

  return new Promise((resolve, reject) => {
    WebAssembly.instantiateStreaming(fetch(path), go.importObject)
    .then(result => {
      go.run(result.instance)
      resolve(result.instance)
    })
    .catch(error => {
      reject(error)
    })
  })
}
```



## Serve the html page

```bash
node index.js
```

Now you have a webapplication listening on `8080`. Because we are on Gitpod, to get the url of the webapplication, type:

```bash
gp url 8080
```

- You should get an url like this one: `https://8080-ivory-worm-lrfo02ph.ws-eu23.gitpod.io`
- Open it in your browser
- Open the console of the browser; you will see this message coming from Go: **"👋 Hello World 🌍"**


## One more thing

So, it's pretty simple to get started, but if you look at the size of `main.wasm`, you'll discover that the size of the generated file is around 2.0M!!! `ls -lh *.wasm` 🙀. Fortunately, we have a friendly solution with TinyGo. Let's see that.

> Tinygo is already installed in this Gitpod project

Get the `wasm_exec.js` file related to **Tinygo** (we are using the version `0.21.0` of Tinygo). Type the below command (in the same directory):

```bash
wget https://raw.githubusercontent.com/tinygo-org/tinygo/v0.21.0/targets/wasm_exec.js
```

Build the wasm file with Tinygo:

```bash
tinygo build -o main.wasm -target wasm ./main.go
```

Check the new size of the wasm file with `ls -lh *.wasm`, you should get something lighter:

```bash
-rwxr-xr-x 1 gitpod gitpod 298K Dec 12 10:31 main.wasm
```

We'll use Tinygo for the next exercises.
