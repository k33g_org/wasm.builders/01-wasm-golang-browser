package main

import (
	"fmt"
)

func main() {
  fmt.Println("👋 Hello World 🌍")
	// prevent the function from returning, 
	// which is required in a wasm module
	<-make(chan bool)
}

