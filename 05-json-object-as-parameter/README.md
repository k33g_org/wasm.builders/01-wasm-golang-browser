# Use a json object as parameter

The objective of this fifth exercise is to create a wasm Golang function that return a json object. And then call it from the web page (from JavaScript) with a **json object as paramater**.

## Complete the source code of `main.go`

Then, update the `Hello` function with the code below:

```golang
func Hello(this js.Value, args []js.Value) interface{} {

	// get an object
	human := args[0]
	// get members of an object
	firstName := human.Get("firstName")
	lastName := human.Get("lastName")

	return map[string]interface{}{
		"message": "👋 Hello " + firstName.String() + " " + lastName.String(),
		"author":  "@k33g_org",
	}

}
```
> **Explanation**:
> - `args[0]` contains the Json object
> - Use the `Get(field_name)` method to retrieve the value of fields

Then, update the JavaScript code of `index.html` with the code below to load the wasm file and call the `Hello` function:

```javascript
loadWasm("main.wasm").then(wasm => {

  let jsonData = Hello({firstName: "Bob", lastName: "Morane"})
  console.log(jsonData)
  document.querySelector("h1").innerHTML = JSON.stringify(jsonData)

}).catch(error => {
  console.log("ouch", error)
}) 
```

## Build the wasm file with Tinygo

```bash
tinygo build -o main.wasm -target wasm ./main.go
```

## Serve the html page

```bash
node index.js
```

Now you have a webapplication listening on `8080`. Because we are on Gitpod, to get the url of the webapplication, type:

```bash
gp url 8080
```

- You should get an url like this one: `https://8080-ivory-worm-lrfo02ph.ws-eu23.gitpod.io`
- Open it in your browser, you should see this message: **{"message":"👋 Hello Bob Morane","author":"@k33g_org"}**

Let's move on to the next exercise.

