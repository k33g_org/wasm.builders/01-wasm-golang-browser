# Interacting with DOM

The objective of this second exercise is to interact with the DOM from the wasm Golang code. We'll add this tag to the html page:

```html
<h2>👋 Hello World 🌍 from TinyGo</h2>
```

## Complete the source code of `main.go`

Import this package `"syscall/js"` 

> **Remark**; This package allows the WebAssembly to access the host environment (the browser).

Then, add this code to the `main` function:

```golang
message := "👋 Hello World 🌍 from TinyGo"

document := js.Global().Get("document")
h2 := document.Call("createElement", "h2")
h2.Set("innerHTML", message)
document.Get("body").Call("appendChild", h2)
```

> **Explanation**:
> - We get a reference to the DOM with `js.Global().Get("document")`
> - We create the `<h2></h2>` element with `document.Call("createElement", "h2")`
> - We set the value of the `innerHTML` with `h2.Set("innerHTML", message)`
> - And finally we add the element to the body with `document.Get("body").Call("appendChild", h2)`

## Build the wasm file with Tinygo

```bash
tinygo build -o main.wasm -target wasm ./main.go
```

## Serve the html page

```bash
node index.js
```

Now you have a webapplication listening on `8080`. Because we are on Gitpod, to get the url of the webapplication, type:

```bash
gp url 8080
```

- You should get an url like this one: `https://8080-ivory-worm-lrfo02ph.ws-eu23.gitpod.io`
- Open it in your browser, you should see this message: **"👋 Hello World 🌍 from TinyGo"**

Now, let's see how to make a callable Go function, that we'll use in our html page.

