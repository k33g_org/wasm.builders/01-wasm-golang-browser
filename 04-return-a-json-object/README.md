# Return a json object

The objective of this fourth exercise is to create a wasm Golang function that return a JavaScript object. And then call it from the web page (from JavaScript).

## Complete the source code of `main.go`

Then, update the `Hello` function with the code below:

```golang
func Hello(this js.Value, args []js.Value) interface{} {

  firstName := args[0].String()
  lastName := args[1].String()

  return map[string]interface{}{
	"message": "👋 Hello " + firstName + " " + lastName,
	"author":  "@k33g_org",
  }

}
```
> This time, we'll pass 2 String parameters to the `Hello` function (`firstName` and `lastName`), and to return a json object we use this type: `map[string]interface{}`:

Then, update the JavaScript code of `index.html` with the code below to load the wasm file and call the `Hello` function:

```javascript
loadWasm("main.wasm").then(wasm => {

  let jsonData = Hello("Bob", "Morane")
  console.log(jsonData)
  document.querySelector("h1").innerHTML = JSON.stringify(jsonData)

}).catch(error => {
  console.log("ouch", error)
}) 
```

## Build the wasm file with Tinygo

```bash
tinygo build -o main.wasm -target wasm ./main.go
```

## Serve the html page

```bash
node index.js
```

Now you have a webapplication listening on `8080`. Because we are on Gitpod, to get the url of the webapplication, type:

```bash
gp url 8080
```

- You should get an url like this one: `https://8080-ivory-worm-lrfo02ph.ws-eu23.gitpod.io`
- Open it in your browser, you should see this message: **{"message":"👋 Hello Bob Morane","author":"@k33g_org"}**

Let's move on to the next exercise.

